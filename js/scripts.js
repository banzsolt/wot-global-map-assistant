$( document ).ready(function() 
{
/*	

	Initialize()
	{
		InitializeParse()
		RunQuery()

		InitializeGlobalMap()
			{
				RunWotAPI()
			}

		NameProvinces()

		MouseClickHandler()
	}


*/

	var NUMBER_OF_CLANS_ON_THE_MAP = 500; //needs to be higher then the number of provinces (467)
	var TOTAL_NUMBER_OF_CLANS_WHO_WHERE_ON_MAP = 500; //needs to be higher then the clans class in parse
	var TOTAL_NUMBER_OF_TANKS_CHECKED = 400; //needs to be bigger then the ammount of entrys in Tanks
	var NUMBERS_OF_REPEATS_FOR_DELETING_DUPLICATED_ELEMENTS = 20; // this has to be hihger than the maximum number of provinces that can be owned by a clan



	var token = "d36cb7cb571852eb22463df8524a443f"; //wargaming token
	var globalMapData = ""; 

	var provincesKeys = [];

	var todaysClans = []; // array of the clans on the global map
	var allProvinces = []; //array of the provinces on the global map
	var selectedProvince // the object of the selected province
	var selectedClan // the obect of the selected clan

	var myProvince = province;
	var l_globalMapData;
	var l_clans;
	var l_tanks;
	var existingClansData = [];
	var globalMapNames = ["DZ_22", "DZ_23", "DZ_24", "NE_07", "NE_06", "NE_05", "NE_04", "NE_03", "NE_02", "NE_01", "CQ_01", "NE_09", "NE_08", "TD_20", "BF_03", "BF_01", "BF_04", "BF_05", "DZ_17", "GH_05", "GH_04", "GH_01", "CG_01", "NE_10", "NE_11", "NE_12", "NE_13", "SL_01", "SL_02", "NG_06", "SN_02", "ML_14", "ML_15", "ML_16", "ML_17", "ML_10", "ML_11", "ML_12", "ML_13", "ML_18", "ML_19", "CF_03", "CF_02", "CF_01", "TG_02", "TG_01", "EG_12", "SD_06", "SD_01", "SD_02", "NG_10", "ML_02", "BJ_02", "CG_02", "NG_11", "MR_17", "CI_05", "SD_29", "ML_04", "MR_05", "MR_07", "MR_06", "MR_10", "SD_16", "MR_09", "MR_08", "SD_10", "SD_23", "SD_22", "GN_03", "GN_02", "GN_01", "NG_08", "TD_19", "TD_18", "TD_11", "TD_10", "TD_13", "TD_12", "TD_15", "TD_14", "TD_17", "TD_16", "SD_24", "MR_18", "MT_01", "MR_16", "BF_02", "MR_14", "MR_15", "MR_12", "MR_13", "LY_19", "GM_01", "CM_02", "CM_03", "SN_01", "CM_01", "CD_01", "CD_03", "CD_02", "TD_08", "TD_09", "TD_02", "TD_03", "TD_01", "TD_06", "TD_07", "TD_04", "TD_05", "SD_30", "LY_22", "LY_21", "LY_20", "CI_02", "CI_03", "CI_01", "CI_06", "CI_04", "BJ_01", "GA_01", "SD_15", "SD_11", "SN_03", "MR_11", "GH_03", "SD_17", "ML_09", "ML_08", "GW_01", "ML_03", "ML_21", "ML_07", "ML_06", "ML_05", "ML_20", "NG_09", "NG_05", "NG_04", "NG_07", "GH_02", "NG_01", "NG_03", "NG_02", "LR_01", "LR_02", "YU_08", "YU_09", "IT_05", "IT_04", "ES_11", "ES_10", "AL_01", "IT_06", "SA_02", "YU_01", "YU_02", "SA_01", "YU_04", "YU_05", "YU_06", "YU_07", "CY_01", "IT_03", "DZ_20", "DZ_21", "DZ_26", "IT_02", "IT_01", "JO_01", "ES_12", "CZ_01", "SA_03", "AM_01", "IT_10", "IT_11", "DZ_15", "IT_13", "IT_14", "ES_03", "DZ_11", "ES_01", "ES_08", "ES_09", "TR_08", "DZ_14", "SY_01", "SA_04", "TR_01", "TR_02", "TR_03", "TR_04", "TR_05", "TR_06", "TR_07", "UA_12", "UA_11", "UA_10", "DZ_13", "GR_05", "GR_04", "GR_03", "GR_02", "HU_03", "HU_02", "DZ_01", "DZ_02", "DZ_03", "DZ_04", "DZ_05", "DZ_06", "DZ_07", "DZ_08", "DZ_09", "EG_11", "EG_10", "EG_15", "EG_14", "UA_08", "UA_09", "UA_01", "UA_03", "UA_04", "UA_06", "UA_07", "RO_01", "CZ_02", "RO_03", "RO_02", "SK_01", "ES_06", "DZ_25", "SA_07", "ML_01", "HU_01", "DZ_10", "EG_08", "EG_09", "EG_02", "EG_03", "EG_01", "EG_06", "EG_07", "EG_04", "EG_05", "MR_04", "LY_09", "LY_08", "MR_03", "MR_02", "LY_05", "LY_04", "LY_07", "LY_06", "LY_01", "LY_03", "LY_02", "FR_11", "FR_10", "FR_13", "FR_12", "FR_15", "FR_14", "FR_17", "FR_16", "SA_05", "FR_18", "MA_05", "GE_01", "MT_01", "BG_01", "BG_03", "BG_02", "LY_18", "LY_16", "LY_17", "LY_14", "LY_15", "LY_12", "LY_13", "LY_10", "LY_11", "FR_02", "FR_03", "FR_01", "FR_06", "FR_07", "ES_07", "FR_04", "FR_05", "FR_08", "FR_09", "DE_10", "DE_11", "RU_21", "RU_20", "RU_22", "RU_40", "AT_03", "AT_02", "IL_01", "CH_01", "MR_01", "IQ_11", "IQ_10", "IR_02", "SA_08", "IR_01", "RU_36", "RU_37", "MD_01", "RU_35", "DZ_16", "ES_04", "RU_38", "RU_39", "AZ_01", "LB_01", "ES_05", "GR_01", "ES_02", "IT_09", "DZ_19", "IT_07", "DZ_12", "JO_02", "IQ_08", "IQ_09", "SA_21", "IQ_04", "IQ_05", "IQ_06", "IQ_07", "IQ_01", "IQ_02", "IQ_03", "IT_08", "SA_22", "RU_03", "SA_18", "IT_12", "RU_05", "SA_11", "SA_13", "SA_12", "SA_15", "SA_14", "MA_01", "MA_02", "MA_03", "MA_04", "AT_01", "DZ_18", "SC_01", "SY_02", "TN_04", "TN_01", "TN_02", "TN_03", "EH_04", "EH_03", "EH_02", "EH_01", "RU_18", "RU_19", "RU_14", "RU_15", "RU_16", "RU_17", "RU_10", "RU_11", "RU_12", "RU_13", "NL_01", "LT_01", "RU_42", "KZ_01", "FI_05", "KZ_10", "NO_05", "NO_04", "NO_06", "NO_01", "NO_03", "NO_02", "UK_06", "UK_07", "UK_06", "UK_05", "UK_02", "UK_03", "UK_01", "UA_02", "EE_01", "RU_44", "BE_01", "DE_01", "DE_03", "DE_02", "DE_05", "DE_04", "DE_07", "DE_06", "DE_09", "DE_08", "UA_05", "SW_01", "SW_02", "SW_03", "SW_04", "SW_05", "SW_06", "SW_07", "RU_47", "RU_46", "RU_45", "RU_41", "RU_43", "RU_49", "RU_48", "RU_25", "RU_24", "RU_23", "RU_29", "RU_28", "RU_50", "RU_51", "RU_53", "RU_58", "DE_12", "RU_34", "RU_32", "RU_33", "RU_30", "RU_31", "RU_27", "RU_26", "FI_03", "FI_02", "FI_01", "DK_02", "DK_01", "FI_04", "RU_02", "RU_01", "RU_07", "RU_06", "RU_04", "RU_09", "RU_08", "BY_06", "BY_05", "BY_04", "BY_03", "BY_02", "BY_01", "IS_01", "PL_06", "PL_07", "PL_04", "PL_05", "PL_02", "PL_03", "PL_01", "PL_08", "LV_01", "LV_02"]


	console.dir(myProvince);

	function InitializeGlobalMap()
	{
		console.log("InitializeGlobalMap() called");

		//SaveGlobalData();

		RunWotAPI();


	}

	//Gets the information from the API
	function RunWotAPI()
	{
		console.log("RunWotAPI() called");

		$.get("https://api.worldoftanks.eu/wot/globalwar/provinces/?application_id=" + token + "&map_id=1", function(data) 
		{
			globalMapData = data;

			console.dir(globalMapData);
		});
	}

	function SaveGlobalData()
	{
		console.log("SaveGlobalData() called");

		var currentdate = new Date(); 
		var date = currentdate.getDate() + "/"
            + (currentdate.getMonth()+1)  + "/" 
            + currentdate.getFullYear() + " @ "  
            + currentdate.getHours() + ":"  
            + currentdate.getMinutes() + ":" 
            + currentdate.getSeconds();


		$.each(globalMapData.data, function(key, value)
		{
			var obj = new Object();
			var save = new Object();
			var entry = new Object();

			var entryNumber = 1;


			save["date"] = date;
			save["clan"] = value.clan_id;

			entry[entryNumber] = save;

			obj["save"] = entry;
			
			delete value["clan_id"];
			obj["province"] = value;

			var GlobalMap = Parse.Object.extend("GlobalMapClans");
			var globalMap = new GlobalMap();

			globalMap.save(
			{
				name: key, 
				province: obj
			}, 
			{
				success: function() {},
					error: function(error) 
					{
						console.warning("There was a probleme while saveing the new datas for Glabal Map Data");
					}
			});

		});
		


	}

	function UpdateGlobalData()
	{

		console.dir("UpdateGlobalData() called");

		var currentdate = new Date(); 
		var date = currentdate.getDate() + "/"
            + (currentdate.getMonth()+1)  + "/" 
            + currentdate.getFullYear() + " @ "  
            + currentdate.getHours() + ":"  
            + currentdate.getMinutes() + ":" 
            + currentdate.getSeconds();


        $.get("https://api.worldoftanks.eu/wot/globalwar/provinces/?application_id=" + token + "&map_id=1", function(data) 
		{
			globalMapData = data;

			console.dir(globalMapData);

			var GlobalMap = Parse.Object.extend("GlobalMapClans");
	    	var query = new Parse.Query(GlobalMap);


			$.each(globalMapData.data, function(key, value)
			{

				for (var i = 0; i < l_globalMapData.length ; ++i)
	    		{	
	    			if(l_globalMapData[i].attributes.name == key)
	    			{
	    				var theProvince = l_globalMapData[i].attributes.province;
	    				var numberOfEntrys = 0;
	    				//Counts how many entrys has the province
						$.each(theProvince.save, function(key, value)
						{
							++numberOfEntrys;
						});

						var object = l_globalMapData[i].attributes.province.save;
						var theFullObject = l_globalMapData[i].attributes.province;
								

						var theEntry = new Object();
						theEntry["date"] = date;
						theEntry["clan"] = value.clan_id;

						object[numberOfEntrys+1] = theEntry;

						console.dir(object);

						theFullObject.save = object;

						l_globalMapData[i].set("province", theFullObject);
						l_globalMapData[i].set("title", theFullObject.province.province_i18n);


	    				l_globalMapData[i].save(
	    				{
 							success: function(myObject) 
 							{
    							// The object was refreshed successfully.
  							},
  							error: function(myObject, error) 
  							{
  								console.warning("there was a probleme adding the todays global map data!!");
  							}
						});
					}


	    		}
				
			});


		}); 


	}


	function InitializeParse()
	{
		console.log("InitializeParse() called");

		Parse.initialize("Uz3wBhBqczHoBIbMgmdnMD79NXHLEqN0ym1YNwpn", "AaYiFQsirwSeIapzOjrq5miu3X0zWAbicvqHCNom");


	}

	function RunQuery()
	{
		console.log("RunQuery() called");

		var GlobalMap = Parse.Object.extend("GlobalMapClans");
	    var query = new Parse.Query(GlobalMap);
	    query.limit(NUMBER_OF_CLANS_ON_THE_MAP);

	    query.find(
	    {
	    	success: function(data)
	    	{
				//Here will be a if statement to check what time was the last entry created
	    		l_globalMapData = data;
	    		console.dir(l_globalMapData);

	    	},
	    	error: function()
	    	{
	    		alert("There was a probleme while requesting the data for the global map.");
	    		console.warning("There was a probleme while requesting the data for the global map.");
	    	}
	    });

	    var Clans = Parse.Object.extend("Clans");
	    var query = new Parse.Query(Clans);
	    query.limit(TOTAL_NUMBER_OF_CLANS_WHO_WHERE_ON_MAP);

	    query.find(
	    {
	    	success: function(data)
	    	{	
	    		l_clans = data;

	    		console.dir(l_clans);
	    	},
	    	error: function(data)
	    	{
	    		console.warning("There was a probleme while requesting the data for all the clans who were on the map.");
	    	}
	    });

	    var Tanks = Parse.Object.extend("Tanks");
	    var query = new Parse.Query(Tanks);
	    query.limit(TOTAL_NUMBER_OF_TANKS_CHECKED);

	    query.find(
	    {
	    	success: function(data)
	    	{	
	    		l_tanks = data;

	    		console.dir(l_tanks);
	    	},
	    	error: function(data)
	    	{
	    		console.warning("There was a probleme while requesting the data for all the clans who were on the map.");
	    	}
	    });


	}

	function Initialize()
	{
		console.log("Initialize() called");
		
		InitializeParse();

		RunQuery();

		InitializeGlobalMap();

		NameProvinces();

		MouseClickHandler();


	}

	function MouseClickHandler()
	{
		console.log("MouseClickHandler() called");


		$(".province").on("click", function()
		{
			var provinceId = $(this).attr("id");
			var number = provinceId.slice(9, provinceId.length)

			console.dir(number);
			console.dir(todaysClans[(number-1)]);
			console.dir(todaysClans);
			console.dir(allProvinces);

			selectedProvince = allProvinces[(number-1)];
			selectedClan = todaysClans[(number-1)];


			if (allProvinces.length == 0)
			{
				selectedProvince = new Object();
			}


			CreateProvinceDetails(selectedProvince);



			console.dir(allProvinces[(number-1)])

		});

		$("#button").on("click", function()
		{
			alert("yes");
			//SaveGlobalData();	//This needs to be called to create the database
			//UpdateGlobalData();	//This needs to be called every single day , this is to get the current clans on the global map
			//UpdateClansDatabase(); //This needs to be called every single day , this is to add the new clans to the database who enterd to the map
			//UpdateTanks(); //This needs to be called to update the tanks requested, but it is not necesarry to update it
			//UpdatePlayers(); //This needs to be called if there is free time to update all the clans
			//TankSummarize(500027038); //This needs to be called for each clan
			DrawLogosOnMap();
			//ReadFile();
			MakeUI();
		})

	}

	function DrawLogosOnMap()
	{
		console.log("DrawLogosOnMap() called");

		//RecursiveDrawing(0);

		for (var i = 0; i< globalMapNames.length; ++i)
		{
			RecursiveDrawing(i);
		}


		function RecursiveDrawing(index)
		{
			if(index == (globalMapNames.length))
			{
				return;
			}
			else
			{
				var myPath = document.getElementById('province-'+(index+1));
				console.log("province-"+(index+1))

				var top = $('#province-'+(index+1)).offset().top;
				var left = $('#province-'+(index+1)).offset().left;
				var width = myPath.getBoundingClientRect().width;
				var height = myPath.getBoundingClientRect().height;

				console.dir(top);
				console.dir(left);
				console.dir(width);
				console.dir(height);

				var GlobalMap = Parse.Object.extend("GlobalMapClans");
		    	var query = new Parse.Query(GlobalMap);

		    	query.equalTo("name", globalMapNames[index]);
				query.first(
				{	
					success: function(object) 
		  			{

		  				var numberOfSaves = 0;

		  				allProvinces[index] = object;

		  				//Count how many saves are in the data
		  				$.each (object.attributes.province.save , function( key, value ) 
						{
							numberOfSaves++;
						});

		  				var clanOnProvince = object.attributes.province.save[numberOfSaves].clan

		  				console.dir(clanOnProvince);

		  				var Clans = Parse.Object.extend("Clans");
		    			var query = new Parse.Query(Clans);
		    			
		    			query.equalTo("clanId", clanOnProvince);
						query.first(
						{
							success: function(object) 
		  					{

		  						console.dir(object);

		  						if(object != undefined)
		  						{
		  							$("#svgmap-logos").append('<img id="logo-' + index + '" widht="24" height="24" style="position: absolute; width: 24px; height: 24px; top:' +  Math.round(top+(height/2))  + 'px ;left:' +  Math.round(left+(width/2)) + 'px; z-index: 2; opacity: 1; margin-left: -12px; margin-top: -12px; cursor: pointer" onclick='+'$("#province-' + (index+1) + '").click();'+' src="'+ object.attributes.name[(object.attributes.name.length - 1)].emblems.large +'">');

		  							todaysClans[index] = object;

		  						}
		  						else
		  						{
		  							$("#svgmap-logos").append('<img id="logo-' + index + '" widht="21" height="24" style="position: absolute; width: 21px; height: 24px; top:' +  Math.round(top+(height/2))  + 'px ;left:' +  Math.round(left+(width/2)) + 'px; z-index: 2; opacity: 1; margin-left: -12px; margin-top: -12px; cursor: pointer" onclick='+'$("#province-' + (index+1) + '").click();'+' src="images/civilian.png">');

		  						}

		  						//RecursiveDrawing(index+1);
		  					},
		  					error: function(error)
		  					{
		  						console.dir("There was an error");
		  					}
		  				});

		  			},
		  			error: function(error) 
		  			{
		  				console.dir("There was an error");
		  			}
				});
			};
		};

	}


	function GetDate()
	{
		var currentdate = new Date(); 
		var date = currentdate.getDate() + "/"
        + (currentdate.getMonth()+1)  + "/" 
        + currentdate.getFullYear();

        return date;
	
	}


	function UpdateTanks()
	{
		console.log("UpdateTanks() called");


		$.get("https://api.worldoftanks.eu/wot/encyclopedia/tanks/?application_id=" + token + "&fields=level,tank_id,short_name_i18n,type_i18n", function(data) 
		{
			var myData = data.data;

			console.dir(myData);

			$.each(myData, function(key, value)
			{
				if (value.level == 10)
				{
					var Tanks = Parse.Object.extend("Tanks");
					var query = new Parse.Query(Tanks);
					

					query.equalTo("tankId", value.tank_id);
					query.first(
					{
			  			success: function(object) 
			  			{
			  				if (object == undefined)
			  				{
			  					//adds a new tank to parse because there is no tank like this in database yet
			  					console.dir("there is no tank like this yet");
			  					console.dir(value);
			  					var tank = new Tanks();

			  					tank.set("tankId", value.tank_id);
			  					tank.set("level", value.level);
			  					tank.set("name", value.short_name_i18n);
			  					tank.set("type", value.type_i18n);

			  					$.get("https://api.worldoftanks.eu/wot/encyclopedia/tankinfo/?application_id=" + token +"&tank_id=" + value.tank_id, function(data)
			  					{
			  						tank.set("icon", data.data[value.tank_id].contour_image);

			  						tank.save();
			  					});

			  					
			  				}
			  				else
			  				{
			  					//update the onject if needed
			  					console.dir("here will be the part if needed to update a tank detail.");

			  					object.set("tankId", value.tank_id);
			  					object.set("level", value.level);
			  					object.set("name", value.short_name_i18n);
			  					object.set("type", value.type_i18n);

			  					$.get("https://api.worldoftanks.eu/wot/encyclopedia/tankinfo/?application_id=" + token +"&tank_id=" + value.tank_id, function(data)
			  					{
			  						object.set("icon", data.data[value.tank_id].contour_image);

			  						object.save();
			  					});

			  				}
			  				
			  			},
			  			error: function(error) 
			  			{
			    			console.dir("not found");
			  			}
					});
				}
			});

		});

	}


	function UpdatePlayers()
	{
		console.log("UpdatePlayers() called");

		var tanks_id = "";

		for (var i = 0; i < l_tanks.length; ++i)
		{
			tanks_id += l_tanks[i].attributes.tankId + ",";
		};


		function SavePlayerToParse(playerId, length, clans)
		{
			console.log("SavePlayerToParse() called");

			console.dir(playerId);

			var searchFor = parseInt(playerId); 


			var Players = Parse.Object.extend("Players");
			var query = new Parse.Query(Players);
			
			query.equalTo("playerId", searchFor);
			query.first(
			{
	  			success: function(object) 
	  			{
	  				console.dir(object);

	  				if (object == undefined)
	  				{
	  					//adds the new player to parse because there is no player like this in database yet
	  					$.get("https://api.worldoftanks.eu/wot/tanks/stats/?application_id=" + token + "&account_id=" + playerId + "&tank_id=" + tanks_id, function(data) 
						{
							
							var myData = data.data[searchFor];

							player = new Players();

							if(data.data[searchFor] != null )
							{
								player.set("numberOfTanks", parseInt(myData.length));

								var obj = new Object();

								for (var i = 0; i < myData.length; ++i)
								{
									var theData = myData[i];
									var tankId = theData.tank_id;

									delete theData.in_garage;
									delete theData.account_id;
									delete theData.tank_id;

									obj[tankId] = theData; 
								};

								player.set("tanks", obj);

							}
							else
							{
								console.log("no tier 10 tank for the player");
								player.set("numberOfTanks", 0);
								//player.set("tanks", "{}");
							}

							player.set("playerId", parseInt(searchFor));	

							$.get("https://api.worldoftanks.eu/wot/account/info/?application_id=" + token + "&account_id=" + searchFor, function(data) 
							{
								player.set("playerName", data.data[searchFor].nickname);

								player.save();

								if(length == 1)
								{
									CreateClansPlayersArray(clans);
								}
							});
						});
	  				}
	  				else
	  				{
	  					//update the player is here
	  					$.get("https://api.worldoftanks.eu/wot/tanks/stats/?application_id=" + token + "&account_id=" + playerId + "&tank_id=" + tanks_id, function(data) 
						{

							var myData = data.data[searchFor];

							player = object;

							if(data.data[searchFor] != null )
							{
								player.set("numberOfTanks", myData.length);

								var obj = new Object();

								for (var i = 0; i < myData.length; ++i)
								{
									var theData = myData[i];
									var tankId = theData.tank_id;

									delete theData.in_garage;
									delete theData.account_id;
									delete theData.tank_id;

									obj[tankId] = theData; 
								};

								player.set("tanks", obj);

							}
							else
							{
								console.log("no tier 10 tank for the player");
								player.set("numberOfTanks", 0);
								//player.set("tanks", "{}");

							}


							player.set("playerId", parseInt(searchFor));
							
							$.get("https://api.worldoftanks.eu/wot/account/info/?application_id=" + token + "&account_id=" + searchFor, function(data) 
							{
								player.set("playerName", data.data[searchFor].nickname);

								player.save();

								if(length == 1)
								{
									CreateClansPlayersArray(clans);
								}
							});

						});

	  				}
	  				
	  			},
	  			error: function(error) 
	  			{
	    			console.dir("not found");
	  			}
			});
		}



		var clans = l_clans;

		console.dir(clans);

		CreateClansPlayersArray(clans);


		function CreateClansPlayersArray(clans)
		{
			console.log("CreateClansPlayersArray() called");

			var playersId = [];

			console.dir(clans);

			if (clans.length > 0)
			{
				$.each ( clans[0].attributes.players, function( key, value ) 
				{
					playersId.push(key);
				});

				clans.shift();

				// console.dir(playersId);

				UpdateTheClansPlayer(playersId, clans);


			}
			else
			{
				
			}
		}



		function UpdateTheClansPlayer(playersId, clans)
		{
			console.log("UpdateTheClansPlayer() called");

			while(playersId.length > 0)
			{
				SavePlayerToParse(playersId[0], playersId.length, clans);

				playersId.shift();
			};

		}

	}


	function TankSummarize(clansId)
	{
		console.log("TankSummarize() called");

		var tanks_id = "";

		for (var i = 0; i < l_tanks.length; ++i)
		{
			tanks_id += l_tanks[i].attributes.tankId + ",";
		};


		var tanks = [];

		var Clans = Parse.Object.extend("Clans");
	    var query = new Parse.Query(Clans);

	    query.equalTo("clanId", clansId);
		query.first(
		{
  			success: function(object) 
  			{
  				var playersObject = object.attributes.players;
  				var playersId = [];
  				var theClan = object;

  				$.each(playersObject, function(key, value)
  				{
  					playersId.push(parseInt(value.account_id));
  				});

  				var savePlayersId = playersId;

  				console.dir(savePlayersId);
  				console.dir(playersId);

  				//Detele the UpdateTheClansPlayer so it will take only from parse
  				UpdateTheClansPlayer(playersId, theClan , function(){});



  			},
  			error: function(error) 
  			{
    			console.warning("Error: " + error.code + " " + error.message);
  			}
		});


		function AddTogetherTanksOfTheClan(theClan)
		{
			console.dir("AddTogetherTanksOfTheClan() called");

			var savePlayersId = [];

			var tanksOfTheClan = new Object();

			$.each(theClan.attributes.players, function(key, value)
			{
				savePlayersId.push(parseInt(key));
			});


			//here is the part when the updateclansplayer is finished
			for (var i = 0; i < l_tanks.length; ++i)
			{
				var obj = new Object();
				var players = [];

				obj["tank_id"] = l_tanks[i].attributes.tankId;
				obj["level"] = l_tanks[i].attributes.level;
				obj["tankName"] = l_tanks[i].attributes.name;
				obj["players"] = players;

				tanksOfTheClan[l_tanks[i].attributes.tankId] = obj;
			}


			var Players = Parse.Object.extend("Players");
			var query = new Parse.Query(Players);

			console.dir(savePlayersId);

			query.containedIn("playerId", savePlayersId);
			query.find(
			{
				success: function(results) 
				{
					console.dir(results);

					for (var i = 0; i < results.length; i++) 
					{ 
						var object = results[i];
						console.dir(object);

						if (object.attributes.numberOfTanks > 0)
						{
							var playersTanks = object.attributes.tanks;

							$.each(playersTanks, function(key, value)
							{
								var obj = new Object();



								obj["playersId"] = object.attributes.playerId;
								obj["playerName"] = object.attributes.playerName;
								//obj["stats"] = value;


								tanksOfTheClan[key].players.push(obj);
							});

						}
					};


					console.dir(tanksOfTheClan);

					theClan.set("tanks", tanksOfTheClan);
					theClan.save();


				},
				error: function(error) 
				{
					alert("Error: " + error.code + " " + error.message);
				}
			});
		}


		function UpdateTheClansPlayer(playersId, theClan, callback)
		{
			console.log("UpdateTheClansPlayer() called");

			if(playersId.length > 0)
			{

				SavePlayerToParse(playersId[0], playersId, theClan);
			}
			else
			{
				AddTogetherTanksOfTheClan(theClan);
			}

			

		}


		function SavePlayerToParse(playerId, playersId, theClan)
		{
			playersId.shift();

			console.log("SavePlayerToParse() called");

			console.dir(playerId);

			var searchFor = parseInt(playerId); 


			var Players = Parse.Object.extend("Players");
			var query = new Parse.Query(Players);
			
			query.equalTo("playerId", searchFor);
			query.first(
			{
	  			success: function(object) 
	  			{
	  				console.dir(object);

	  				if (object == undefined)
	  				{
	  					//adds the new player to parse because there is no player like this in database yet
	  					$.get("https://api.worldoftanks.eu/wot/tanks/stats/?application_id=" + token + "&account_id=" + playerId + "&tank_id=" + tanks_id, function(data) 
						{
							
							var myData = data.data[searchFor];

							player = new Players();

							if(data.data[searchFor] != null )
							{
								player.set("numberOfTanks", parseInt(myData.length));

								var obj = new Object();

								for (var i = 0; i < myData.length; ++i)
								{
									var theData = myData[i];
									var tankId = theData.tank_id;

									delete theData.in_garage;
									delete theData.account_id;
									delete theData.tank_id;

									obj[tankId] = theData; 
								};

								player.set("tanks", obj);

							}
							else
							{
								console.log("no tier 10 tank for the player");
								player.set("numberOfTanks", 0);
								//player.set("tanks", "{}");
							}

							player.set("playerId", parseInt(searchFor));		



							$.get("https://api.worldoftanks.eu/wot/account/info/?application_id=" + token + "&account_id=" + searchFor, function(data) 
							{
								player.set("playerName", data.data[searchFor].nickname);

								player.save(
								{
		 							success: function(myObject) 
		 							{
		    							UpdateTheClansPlayer(playersId, theClan, function(){})
		  							},
		  							error: function(myObject, error) 
		  							{
		  								console.warning("dd");
		  							}
								});
							});

						});
	  				}
	  				else
	  				{
	  					//update the player is here
	  					$.get("https://api.worldoftanks.eu/wot/tanks/stats/?application_id=" + token + "&account_id=" + playerId + "&tank_id=" + tanks_id, function(data) 
						{

							var myData = data.data[searchFor];

							player = object;

							if(data.data[searchFor] != null )
							{
								player.set("numberOfTanks", myData.length);

								var obj = new Object();

								for (var i = 0; i < myData.length; ++i)
								{
									var theData = myData[i];
									var tankId = theData.tank_id;

									delete theData.in_garage;
									delete theData.account_id;
									delete theData.tank_id;

									obj[tankId] = theData; 
								};

								player.set("tanks", obj);

							}
							else
							{
								console.log("no tier 10 tank for the player");
								player.set("numberOfTanks", 0);
								//player.set("tanks", "{}");

							}


							player.set("playerId", searchFor);
							
							$.get("https://api.worldoftanks.eu/wot/account/info/?application_id=" + token + "&account_id=" + searchFor, function(data) 
							{
								player.set("playerName", data.data[searchFor].nickname);

								player.save(
								{
		 							success: function(myObject) 
		 							{
		    							UpdateTheClansPlayer(playersId, theClan, function(){})
		  							},
		  							error: function(myObject, error) 
		  							{
		  								console.warning("dd");
		  							}
								});
							});

						});

	  				}
	  				
	  			},
	  			error: function(error) 
	  			{
	    			console.dir("not found");
	  			}
			});
		}

	}

	//This needs to be run after we got the todays global map data (RunWotAPI function)
	function UpdateClansDatabase()
	{
		console.log("UpdateClansDatabase() called");

		provinces = globalMapData.data;

		console.dir(provinces);

		var todaysClansId = [];
		var allClansId = [];

		//existing clans
		for (var i = 0; i < l_clans.length ; ++i)
		{
			allClansId.push(l_clans[i].attributes.clanId);
		};

		//create an array of the todays clans who are on the map
		$.each(globalMapData.data, function(key, value)
		{
			if (value.clan_id == null)
			{
				console.log("<---------- there is a province with no owner : " + value.province_i18n + " ---------->");
			}
			else
			{
				todaysClansId.push(value.clan_id);
			}

		});

		//delete the same clans from the array
		for(var i = 0; i < NUMBERS_OF_REPEATS_FOR_DELETING_DUPLICATED_ELEMENTS; ++i)
		{
			$.unique(todaysClansId);
		}

		var newAllClansId = allClansId.concat(todaysClansId);

		//delete the same clans again
		for(var i = 0; i < NUMBERS_OF_REPEATS_FOR_DELETING_DUPLICATED_ELEMENTS; ++i)
		{
			$.unique(newAllClansId);
		};


		newAllClansId.sort();

		var requests = [];
		var requestedClanList = "";


		//Creates the requests
		for (var i = 0; i < newAllClansId.length; ++i)
		{
			requestedClanList += newAllClansId[i] + ",";

			if ((i % 100) == 99)
			{
				requests.push(requestedClanList);

				requestedClanList = "";
			}
			else
			{
				if ( i == (newAllClansId.length - 1) )
				{
					requests.push(requestedClanList)

					requestedClanList = "";
				}
			}
		};

		var Clans = Parse.Object.extend("Clans");
		var query = new Parse.Query(Clans);


		console.dir(requests);
		ClanDetailsRecursion(requests);


		function ClanDetailsRecursion(requests)
		{
			console.log("ClanDetailsRecursion() called");

			if (requests.length > 0)
			{
				RequestClansDetails(requests[0], function(data)
				{
					console.dir("continues");

					requests.shift();

					$.each(data.data, function(key, value)
					{
						existingClansData.push(value);
					});

					ClanDetailsRecursion(requests);
				});
			}
			else
			{
				SaveClansDataWithTheNewData();
			}
		}

		function RequestClansDetails(requestedClanList, callback)
		{
			console.log("RequestClansDetails() called");

			var result;

			$.get("https://api.worldoftanks.eu/wot/clan/info/?application_id=" + token + "&clan_id=" + requestedClanList, function(data) 
			{
				result = data;

				console.dir(result);

				callback(result);
			});

		}

		function SaveClansDataWithTheNewData()
		{
			console.log("SaveClansDataWithTheNewData() called");

			RecursiveSaving(0);


			function RecursiveSaving(index)
			{

				console.log("RecursiveSaving() called");

				if(index < existingClansData.length)
				{
					var Clans = Parse.Object.extend("Clans");
					var query = new Parse.Query(Clans);

					query.equalTo("clanId", existingClansData[index].clan_id);
					query.first(
					{
						success: function(object) 
						{
							if(object != undefined)
							{
								//save it only if changed
								console.dir(object);

								object.set("currentName", existingClansData[index].name)

								var array = [];
								var obj = new Object();

								obj["abbreviation"] = existingClansData[index].abbreviation;
								obj["color"] = existingClansData[index].color;
								obj["date"] = GetDate();
								obj["emblems"] = existingClansData[index].emblems;
								array.push(obj);

								object.set("name", array);
								object.set("players", existingClansData[index].members)

								$.get("https://api.worldoftanks.eu/wot/clan/provinces/?application_id=" + token + "&clan_id=" + existingClansData[index].clan_id , function(data)
								{
									object.set("provinces", data);
									object.save();
									RecursiveSaving(index+1);
								});

								console.dir("that is the object what already exists");
							}
							else
							{
								clan = new Clans();

								clan.set("clanId", existingClansData[index].clan_id);
								clan.set("currentName", existingClansData[index].name);
								

								var array = [];
								var obj = new Object();

								obj["abbreviation"] = existingClansData[index].abbreviation;
								obj["color"] = existingClansData[index].color;
								obj["date"] = GetDate();
								obj["emblems"] = existingClansData[index].emblems;
								array.push(obj);

								clan.set("name", array);

								clan.set("players", existingClansData[index].members);

								$.get("https://api.worldoftanks.eu/wot/clan/provinces/?application_id=" + token + "&clan_id=" + existingClansData[index].clan_id , function(data)
								{
									clan.set("provinces", data);
									clan.save();
									RecursiveSaving(index+1);
								});
							}
							
						},
						error: function(error) 
						{
							console.warning("Error: " + error.code + " " + error.message);
						}
					});
				}
				else
				{
					console.dir("All calans updated");
				}

			}
				
		};

	}


	function NameProvinces()
	{
		console.log("NameProvinces() called");

		var index = 0;

		$("svg").find("path").each(function()
		{
			index++;

			var province = "province-" + index;

			$(this).attr("id", province);
			$(this).attr("class", "province");

		})

	}

	function ReadFile () 
	{
		console.log("ReadFile() called");

		var file;
		var path;
		var isCW = true;

		UploadToMyServer();

		function UploadToMyServer()
		{
			var input = document.getElementById("images"), 
			formdata = false;  

			if (window.FormData) 
			{
				console.dir(FormData);
		  		formdata = new FormData();
			}
			
		 	input.addEventListener("change", function (evt) 
		 	{

		 		var i = 0, len = this.files.length, img, reader, file;
			
				for ( ; i < len; i++ ) 
				{
					file = this.files[i];
			

					if ( window.FileReader ) 
					{
						reader = new FileReader();
						reader.onloadend = function (e) 
						{ 
							console.dir(file.name);
						};
						reader.readAsDataURL(file);
					}
					if (formdata) 
					{
						formdata.append("images[]", file);
					}
				};
			
				if (formdata) 
				{
					$.ajax({
						url: "php/upload.php",
						type: "POST",
						data: formdata,
						processData: false,
						contentType: false,
						success: function (res) 
						{
							console.dir(res);
							AfterUplaodedToMyServer();
						}
					});
				}
			}, false);
		}

      	function AfterUplaodedToMyServer()
      	{
      		console.log("AfterUplaodedToMyServer() called");

      		file = $("#images")[0].files[0];


      		var serverUrl = 'https://api.parse.com/1/files/' + file.name;


			//Saving the file to parse
			$.ajax(
			{
				type: "POST",
				beforeSend: function(request) 
				{
					request.setRequestHeader("X-Parse-Application-Id", 'Uz3wBhBqczHoBIbMgmdnMD79NXHLEqN0ym1YNwpn');
					request.setRequestHeader("X-Parse-REST-API-Key", 'ZSn67z4iLpC9iy8qSoVV37Nxxlw4MZCez8nnze4e');
					request.setRequestHeader("Content-Type", file.type);
				},
				url: serverUrl,
				data: file,
				processData: false,
				contentType: false,
				success: function(data) 
				{
				  alert("File available at: " + data.url);

				  var replay = new Parse.Object("Replays");
				  replay.set("fileUrl", data.url);

				  path = "uploads/" + file.name;

				  path = path.slice(0,path.length-9);

				  path += "json";

				  var result;

				  var rawFile = new XMLHttpRequest();
				  rawFile.open("GET", path, false);
				  rawFile.onreadystatechange = function ()
				  {
				    if(rawFile.readyState === 4)
					  {
						if(rawFile.status === 200 || rawFile.status == 0)
						{
							var allText = rawFile.responseText;
							result = JSON.parse(allText);

							SetTheValuesOfReplay(replay, result);

							console.dir(isCW);

							if(isCW == true)
							{
								replay.save(
								{
									success: function() 
									{
										console.dir("Done");

									},
									error: function(error) 
									{
										console.warning("Something went wrong");
									}
								});

							}
						}
					  }
				  }
				  rawFile.send(null);

				  console.dir(result);


				},
				error: function(data) 
				{
				  var obj = jQuery.parseJSON(data);
				  alert(obj.error);
				}
				});


      	}

      	function SetTheValuesOfReplay(replay, object)
      	{
      		console.log("SetTheValuesOfReplay() called");

      		console.dir(object);

      		replay.set("data", object);
      		replay.set("date", object.identify.arenaCreateTime);

      		var clan1 = [];
      		var clan1Id;
      		var clan1Name;
      		var clan2 = [];
      		var clan2Id;
      		var clan2Name;
      		var clan1Deaths = new Object();
      		var clan2Deaths = new Object();
      		var vehicleid;

      		$.each(object.datablock_battle_result.players, function(key, value)
      		{

      			if (clan1.length == 0)
      			{
      				console.dir(key);
      				clan1.push(parseInt(key));
      				clan1Id = value.clanDBID;
      				clan1Name = value.clanAbbrev;

      				vehicleid = value.vehicleid;

      				if(object.datablock_battle_result.vehicles[vehicleid].health < 1)
      				{
      					clan1Deaths[key.toString()] = object.datablock_battle_result.vehicles[vehicleid].typeCompDescr;
      				}

      			}
      			else
      			{
      				if (value.clanDBID == clan1Id)
      				{
      					clan1.push(parseInt(key));

      					vehicleid = value.vehicleid;

	      				if(object.datablock_battle_result.vehicles[vehicleid].health < 1)
	      				{
	      					clan1Deaths[key.toString()] = object.datablock_battle_result.vehicles[vehicleid].typeCompDescr;
	      				}
      				}
      				else
      				{
      					if (clan2.length == 0)
      					{
      						clan2.push(parseInt(key));
      						clan2Id = value.clanDBID;
      						clan2Name = value.clanAbbrev;

      						vehicleid = value.vehicleid;

		      				if(object.datablock_battle_result.vehicles[vehicleid].health < 1)
		      				{
		      					clan2Deaths[key.toString()] = object.datablock_battle_result.vehicles[vehicleid].typeCompDescr;
		      				}
      					}
      					else
      					{
      						if ( value.clanDBID == clan2Id)
      						{
      							clan2.push(parseInt(key));

      							vehicleid = value.vehicleid;

			      				if(object.datablock_battle_result.vehicles[vehicleid].health < 1)
			      				{
			      					clan2Deaths[key.toString()] = object.datablock_battle_result.vehicles[vehicleid].typeCompDescr;
			      				}
      						}
      						else
      						{
      							alert("Please upload a replay from clan wars.");
      							isCW = false;
      							return false;
      						}
      					}
      				}
      			}

      		});

      		replay.set("clan1Name", clan1Name);
      		replay.set("clan2Name", clan2Name);
      		replay.set("clan1", clan1Id);
      		replay.set("clan2", clan2Id);
      		replay.set("clan1Players", clan1);
      		replay.set("clan2Players", clan2);
      		replay.set("clan1Deaths", clan1Deaths);
      		replay.set("clan2Deaths", clan2Deaths);

      	}


	}


	function MakeUI()
	{
		console.log("MakeUI() called");

		CreateOwnersTanksList(500027038);

		CreateProvinceDetails(province);

	}


	function CreateOwnersTanksList(ownerId)
	{

		console.log("CreateOwnersTanksList() called");

		var ownersClans;

		var Clans = Parse.Object.extend("Clans");
		var query = new Parse.Query(Clans);

		query.equalTo("clanId", ownerId);
		query.first(
		{	
			success: function(object) 
				{
					ownersClans = object.attributes.tanks;
					Continue1(ownersClans);
				},
				error: function(error)
				{
					console.dir("There was a probleme while trying to get the clan's tanks");
				}
		});

		function Continue1(ownersClans)
		{
			console.log("Continue1() called");

			var index = 0;

			$.each(ownersClans, function(key, value)
			{
				++index;

				RequestIcons(key, value, index);
			});


			function RequestIcons(key, value, index)
			{
				console.log("RequestIcons() called");

				var Tanks = Parse.Object.extend("Tanks");
				var query = new Parse.Query(Tanks);

				query.equalTo("tankId", parseInt(key));
				query.first(
				{	
					success: function(object) 
						{
							var innerHTML = '<li><img src="' + object.attributes.icon + '">' + value.players.length + '<ul>';

							for ( var i = 0; i < value.players.length; ++i)
							{
								innerHTML += '<li>' + value.players[i].playerName + '</li>';
							}

							innerHTML += "</ul></li>";

							$("#owners-tanks").append(innerHTML);

							if(index == l_tanks.length)
							{
								//this is called from plugins/expendable
								prepareList();
							}
						},
						error: function(error)
						{
							console.dir("There was a probleme while trying to get the tanks icon");
						}
				});
			}
		}

	}

	function CreateProvinceDetails(province)
	{
		console.log("CreateProvinceDetails() called");

		var provinceDetails = province.attributes.province.province;
		var selectedClansNameDetails = selectedClan.attributes.name[(selectedClan.attributes.name.length - 1)];

		$("#province-name").html(provinceDetails.province_i18n);
		$("#province-map").html(provinceDetails.arena);
		$("#province-income").html(provinceDetails.revenue);

		$("#province-owners-logo").html('<img src=' + selectedClansNameDetails.emblems.large + '>');
		$("#province-owners-abbreviation").html(selectedClansNameDetails.abbreviation);
		$("#province-owners-name").html(selectedClan.attributes.currentName);

	}




	Initialize();

});
